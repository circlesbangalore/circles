package com.circles.testScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.circles.constants.CirclesConstants;
import com.circles.locators.Locator;
import com.circles.pages.Pages;
import com.framework.commonutils.CommonFunction;
import com.framework.commonutils.UIActions;
import com.framework.main.AutomationBase;


public class VerifyFacebookLoginAndPostCommentScript extends AutomationBase {
	Pages pages = PageFactory.initElements(driver, Pages.class);
	String path = propRead.readPropertyFile("project.properties", "userdetail");
	String sheetName = "pincode_QISpine";
	String exlData = null;
	String url = "CIRCLESFACEBOOKURL";
	

	@BeforeTest
	public void setUp() {
		try {
			logger.info("STARTED: PinCode Automation");
			logger.info("Running BeforeTest");
			super.setUp(path, sheetName, url);
			logger.info("@BeforeTest: Setup done");
			PageFactory.initElements(driver, VerifyFacebookLoginAndPostCommentScript.class);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("@BeforeTest: Setup failed");
		}
	}

	/*
	 * @AfterTest public void teardown() { logger.info("Running AfterTest");
	 * super.teardown(); }
	 */
	

	@Test
	
	public void invokeTest() throws Exception {
	
		//Script to Post a Comment in Facebook//
		Thread.sleep(5000);
		logger.info("Loading Offer Page");	
		
		pages.facebookPage().enterText(Locator.FaceBookPage.TXT_EMAIL_ID, CirclesConstants.FACEBOOK_EMAILID);
		pages.facebookPage().enterText(Locator.FaceBookPage.TXT_PASSWORD, CirclesConstants.FACEBOOK_PASSWORD);
		pages.facebookPage().clickElement(Locator.FaceBookPage.BTN_LOGIN);
		
		pages.facebookPage().clickElement(Locator.FaceBookPage.TAB_HOME);
		pages.facebookPage().clickElement(Locator.FaceBookPage.TAB_COMPOSE_POST);
		pages.facebookPage().clickElement(Locator.FaceBookPage.TAB_COMPOSE_POST);
		pages.facebookPage().clickElement(Locator.FaceBookPage.TXT_AREA_COMPOSE_POST);
		String newPost = CommonFunction.generatingRandomString(8);
		pages.facebookPage().enterText(Locator.FaceBookPage.TXT_AREA_COMPOSE_POST, newPost);
		pages.facebookPage().clickElement(Locator.FaceBookPage.BTN_POST);
		
		
		//Below script is to Verify posted comment in Facebook//
		
		pages.facebookPage().clickElement(Locator.FaceBookPage.TAB_HOME);
		Thread.sleep(5000);		
		
		String preXpath = "//*[contains(text(),'";
		String postXpath = "')]";
		String xpathExpression = preXpath + newPost + postXpath;
	    String postedComment = driver.findElement(By.xpath(xpathExpression));    		
	    		
		UIActions.VerifyText("To Verify Comment Posted", "Text Verification", newPost, postedComment);	
	
		
	}
	
	

}
