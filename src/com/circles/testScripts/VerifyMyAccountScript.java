package com.circles.testScripts;


import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.circles.constants.CirclesConstants;

import com.circles.locators.Locator;
import com.circles.pages.Pages;
import com.framework.commonutils.ExcelUtils;
import com.framework.commonutils.UIActions;
import com.framework.main.AutomationBase;


public class VerifyMyAccountScript extends AutomationBase {
	Pages pages = PageFactory.initElements(driver, Pages.class);
	String path = propRead.readPropertyFile("project.properties", "userdetail");
	String sheetName = "";
	String exlData = null;
	String url = "CIRCLESURL";
	

	@BeforeTest
	public void setUp() {
		try {
			logger.info("STARTED: PinCode Automation");
			logger.info("Running BeforeTest");
			super.setUp(path, sheetName, url);
			logger.info("@BeforeTest: Setup done");
			PageFactory.initElements(driver, VerifyMyAccountScript.class);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("@BeforeTest: Setup failed");
		}
	}

	/*
	 * @AfterTest public void teardown() { logger.info("Running AfterTest");
	 * super.teardown(); }
	 */
	

	@Test
	//@Parameters({ "QISPINEURL" })
	public void invokeTest() throws Exception {
		ExcelUtils.setExcelFile(path, sheetName);
		Thread.sleep(5000);
		logger.info("Loading Offer Page");	
		pages.circlesHomePage().waitForPageLoad(5);
		pages.circlesHomePage().clickElement(Locator.CirclesHomePage.LNK_SIGNUP);
		pages.circlesSignInPage().waitForElementPresent(5, Locator.CirclesSignInPage.BTN_SIGNIN);
		pages.circlesSignInPage().enterText(Locator.CirclesSignInPage.TXT_EMAIL, CirclesConstants.EMAILID);
		pages.circlesSignInPage().enterText(Locator.CirclesSignInPage.TXT_PASSWORD, CirclesConstants.PASSWORD);
		pages.circlesSignInPage().clickElement(Locator.CirclesSignInPage.BTN_SIGNIN);
		pages.circlesPlanPage().waitForElementPresent(10, Locator.CirclesPlanPage.TXT_GET_IT_NOW);
		pages.circlesPlanPage().clickElement(Locator.CirclesPlanPage.TAB_MY_ACCOUNT);
		String email = pages.circlesMyAccountPage().getText(Locator.CirclesMyAccountPage.TXT_EMAIL);
		

		//To Verify Email Adderss in My Account page
		
		UIActions.VerifyText("To Verify Email Address", "Text Verification", CirclesConstants.EMAILID, email);
		
		
		
		
		
	}
	
	

}
