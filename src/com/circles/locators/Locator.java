package com.circles.locators;

import org.openqa.selenium.By;


public class Locator {

	public static final class CirclesHomePage {
		
		public static final By LNK_SIGNUP = By.xpath("//*[contains(text(),'Sign up')]");	
		
	}
	
	public static final class CirclesSignInPage {		
		public static final By TAB_SIGNIN = By.xpath("//*[contains(text(),'SIGN IN')]");		
		public static final By TXT_EMAIL = By.name("email");
		public static final By TXT_PASSWORD= By.name("password");
		public static final By BTN_SIGNIN = By.xpath("//*[@class='btn btn-primary btn-lg btn-block Button']");	
		public static final By BTN_SIGNIN_FACEBOOK = By.xpath("//*[@id='st-container']/div/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div[1]/span/button");
		public static final By TXT_FACEBOOK_EMAIL = By.name("email");
		public static final By TXT_FACEBOOK_PASSWORD = By.name("pass");
		public static final By BTN_FACEBOOK_LOGIN = By.name("login");
		
	}
	
	public static final class CirclesPlanPage {				
		public static final By TXT_GET_IT_NOW = By.xpath("//*[contains(text(),'Get it now')]");			
		public static final By TAB_MY_ACCOUNT = By.xpath("//*[contains(text(),'MY ACCOUNT')]");
		
	}
	
	public static final class CirclesMyAccountPage {			
		public static final By TXT_EMAIL = By.xpath("//*[@id='st-container']/div/div/div[2]/div[1]/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/form/div[3]/div/input");		
		
	}
	
	public static final class FaceBookPage {		
		public static final By TXT_EMAIL_ID = By.id("email");			
		public static final By TXT_PASSWORD = By.id("pass");
		public static final By BTN_LOGIN = By.id("loginbutton");
		public static final By TAB_HOME = By.xpath("//*[@id='u_0_c']/a");
		public static final By TAB_COMPOSE_POST = By.xpath("//*[contains(text(),'Compose Post')]");
		public static final By TXT_AREA_COMPOSE_POST = By.xpath("//*[@id='js_rr']/div[1]/div/div[1]/div[2]/div/div/div");
		public static final By BTN_POST = By.xpath("//*[@id='js_8p']/div[2]/div[3]/div/div[2]/div/span[2]/button");	
		
	}

	
}
