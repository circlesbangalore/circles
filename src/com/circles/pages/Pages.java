package com.circles.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class Pages {
	static WebDriver driver;


	
	
	CirclesHomePage circlesHomePage = PageFactory.initElements(driver, CirclesHomePage.class);
	CirclesSignInPage circlesSignInPage = PageFactory.initElements(driver, CirclesSignInPage.class);
	CirclesPlanPage circlesPlanPage = PageFactory.initElements(driver, CirclesPlanPage.class);
	CirclesMyAccountPage circlesMyAccountPage = PageFactory.initElements(driver, CirclesMyAccountPage.class);
	FaceBookPage facebookPage = PageFactory.initElements(driver, FaceBookPage.class);
	
	

	public Pages(WebDriver driver) {
		Pages.driver = driver;
	}

	
	
	public CirclesHomePage circlesHomePage() {
		circlesHomePage = new CirclesHomePage(Pages.driver);
		return circlesHomePage;		
	}
	
	public CirclesSignInPage circlesSignInPage() {
		circlesSignInPage = new CirclesSignInPage(Pages.driver);
		return circlesSignInPage;		
	}
	
	public CirclesPlanPage circlesPlanPage() {
		circlesPlanPage = new CirclesPlanPage(Pages.driver);
		return circlesPlanPage;		
	}
	
	public CirclesMyAccountPage circlesMyAccountPage() {
		circlesMyAccountPage = new CirclesMyAccountPage(Pages.driver);
		return circlesMyAccountPage;		
	}
	
	
	public FaceBookPage facebookPage() {
		facebookPage = new FaceBookPage(Pages.driver);
		return facebookPage;		
	}	
	
	
	
	
}
