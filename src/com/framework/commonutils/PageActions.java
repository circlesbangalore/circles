package com.framework.commonutils;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public abstract class PageActions {

	protected UIActions action;
	public final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	public static PropertyFileRead propRead = new PropertyFileRead();

	public boolean enterText(By element, String txtValue) {
		try {
			action.sendText(element, txtValue);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean sendKeys(Keys Key) {
		try {
			action.pressActionsKey(Key);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean clearText(By element) {
		try {
			action.clearText(element);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean enterTextToUpload(By element, String txtValue) {
		try {
			action.sendTextUploadFile(element, txtValue);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean clickElement(By element) {
		try {
			action.clickElement(element);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public boolean elementVisible(By element) {
		try {
			action.elementIsVisible(element);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	public boolean elementEnabled(By element) {
		try {
			action.elementIsEnabled(element);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getText(By element) {
		String text = "";
		try {
			text = action.getElementText(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return text;
	}

	public String getReadOnlyText(By element) throws NoSuchElementException {
		String text = "";
		try {
			text = action.getReadOnlyText(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return text;

	}

	public List<WebElement> getListofElements(By by) {
		List<WebElement> elementList = null;
		try {
			elementList = action.getListofElement(by);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return elementList;
	}

	public boolean pressKeyboardKey(String key) {
		try {
			switch (key) {
			case "ENTER":
				action.pressActionsKey(Keys.ENTER);
				break;
			case "ESC":
				action.pressActionsKey(Keys.ESCAPE);
				break;
			case "UP":
				action.pressActionsKey(Keys.ARROW_UP);
				break;
			case "DOWN":
				action.pressActionsKey(Keys.ARROW_DOWN);
				break;
			case "TAB":
				action.pressActionsKey(Keys.TAB);
				break;
			case "CONTROL":
				action.pressActionsKey(Keys.CONTROL);
				break;
			case "DELETE":
				action.pressActionsKey(Keys.DELETE);
				break;
			default:
				break;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean deleteCookies() {
		return action.deleteCookie();
	}
	
	
	public boolean waitForElementPresent(long second, By element) {
		return action.waitForElement(second, element);
	}

	
	public boolean waitForElementClickable(long second, By element) {
		return action.waitForElementToBeClickable(second, element);
	}

	public boolean waitForElementToDisapper(long second, By element) {
		return action.waitForElementToDisapper(second, element);
	}

	public boolean waitForPageLoad(long second) {
		return action.waitForPageLoad(second);
	}

	public boolean selectDropdownByText(By element, String txtValue) {
		try {
			action.selectDropDownByVisibleText(element, txtValue);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean selectDropdownByValue(By element, int index) {
		try {
			action.selectDropDownByIndex(element, index);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getSelectedDropDownValue(By element) {
		try {
			return action.getSelectedOption(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return "[EXCEPTION] getSelectedDropDownValue() Exception Occured";
		}
	}

	public List<String> getSelectableOptions(By element) {
		try {
			return action.getSelectableOptions(element);
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean popupAccept() throws Exception {
		try {
			action.acceptPopUpAlert();
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean popupDissmiss() throws Exception {
		try {
			action.dismissPopUpAlert();
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String getPopUpText() throws Exception {
		try {
			return action.getPopUpAlertText();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return "[EXCEPTION] getPopUpText() Exception Occured";
		}
	}

	public boolean switchFrameByName(String frameName) {
		try {
			action.switchFrameByName(frameName);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean switchFrameByWebElement(WebElement frameElement) {
		try {			
			action.switchFrameByWebElement(frameElement);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean switchToDefaultContext() {
		try {
			action.switchToDefaultContent();
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean switchToWindow(String windowHandle) {
		try {
			action.switchToWindow(windowHandle);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean clickElementByJavacript(By by) {
		try {
			action.clickElementByJavaScript(by);
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}
}
